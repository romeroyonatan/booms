import sqlalchemy
from sqlalchemy import orm


DB_CONNECTION_STRING = "postgres://postgres:postgresql@localhost:55432/"

engine = sqlalchemy.create_engine(DB_CONNECTION_STRING)
Session = orm.sessionmaker(bind=engine)
session = Session()
