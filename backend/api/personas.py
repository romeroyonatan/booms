import logging

from connexion import NoContent

from sqlalchemy.orm import exc

from db import session
from models import Persona


logger = logging.getLogger(__name__)


def post(persona):
    logger.info("Creando persona %r", persona)
    obj = Persona(**persona)
    session.add(obj)
    session.commit()
    return dict(persona), 201


def put(id, persona):
    query = session.query(Persona).filter_by(id=id)
    try:
        obj = query.one()
    except exc.NoResultFound:
        return NoContent, 404
    else:
        for field in persona:
            setattr(obj, field, persona[field])
        session.commit()
        return persona


def delete(id):
    query = session.query(Persona).filter_by(id=id)
    try:
        persona = query.one()
    except exc.NoResultFound:
        return NoContent, 404
    else:
        session.delete(persona)
        session.commit()
        return NoContent, 204


def get(id):
    query = session.query(Persona).filter_by(id=id)
    try:
        persona = query.one()
    except exc.NoResultFound:
        return NoContent, 404
    else:
        data = dict(query.one())
        data["secciones"] = [dict(seccion) for seccion in persona.secciones]
        return data


def search(per_page, page, q=None):
    query = session.query(Persona).order_by("id")
    if q:
        query = query.filter(Persona.nombre.ilike(f"%{q}%"))
    if query.count() > per_page:
        offset = per_page * page
        paginate = slice(offset, offset + per_page)
    else:
        paginate = slice(0, query.count())
    return [dict(persona) for persona in query.all()[paginate]]
