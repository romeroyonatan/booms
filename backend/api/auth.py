# our hardcoded mock "Bearer" access tokens
TOKENS = {"123": "jdoe", "456": "rms"}


def token_info(access_token) -> dict:
    profile = TOKENS.get(access_token)
    if not profile:
        return None
    return {"profile": profile, "scope": ["profile"]}
