import logging

from connexion import NoContent

from sqlalchemy.orm import exc

from db import session
from models import Seccion


logger = logging.getLogger(__name__)


def post(seccion):
    logger.debug("Creando seccion %r", seccion)
    obj = Seccion(**seccion)
    session.add(obj)
    session.commit()
    logger.info("Seccion creada id=%d seccion=%r", obj.id, dict(obj))
    return dict(obj), 201


def put(id, seccion):
    updated = session.query(Seccion).filter_by(id=id).update(values=seccion)
    # no se actualizaron registros
    if updated == 0:
        return NoContent, 404
    session.commit()
    logger.info("Seccion actualizada id=%d seccion=%r", id, seccion)
    return seccion


def delete(id):
    session.query(Seccion).filter_by(id=id).delete()
    session.commit()
    return NoContent, 204


def get(id):
    query = session.query(Seccion).filter_by(id=id)
    try:
        seccion = query.one()
    except exc.NoResultFound:
        return NoContent, 404
    else:
        data = dict(query.one())
        data["personas"] = [dict(persona) for persona in seccion.personas]
        return data


def search(per_page, page):
    query = session.query(Seccion).order_by("id")
    if query.count() > per_page:
        offset = per_page * page
        paginate = slice(offset, offset + per_page)
    else:
        paginate = slice(0, query.count())
    return [dict(seccion) for seccion in query.all()[paginate]]
