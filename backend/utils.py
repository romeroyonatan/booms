import decimal
import datetime


def alchemyencoder(obj):
    """JSON encoder function for SQLAlchemy special classes."""
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)


def to_dict(obj, fields=None):
    if not fields:
        fields = set(field for field in vars(obj) if not field.startswith("_"))
    return {field: getattr(obj, field) for field in fields}
