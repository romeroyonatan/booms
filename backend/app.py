#!/usr/bin/env python3
import logging

import connexion

from flask_cors import CORS

logging.basicConfig(level=logging.INFO)

app = connexion.FlaskApp(__name__)
app.add_api('swagger/api_v1.yaml',
            arguments={'title': 'API personas'},
            strict_validation=True)

# add CORS support
CORS(app.app)

app.run(port=9090)
