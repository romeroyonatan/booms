from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    String,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


Base = declarative_base()


class IterableMixin:
    def __iter__(self):
        fields = (field for field in vars(self)
                  if not field.startswith("_"))
        return ((field, getattr(self, field)) for field in fields)


class Persona(IterableMixin, Base):
    __tablename__ = 'personas'

    id = Column(Integer, primary_key=True)
    nombre = Column(String, nullable=False)
    fecha_nacimiento = Column(Date)
    dni = Column(String(10))
    email = Column(String(64))
    telefono = Column(String(16))
    celular = Column(String(16))
    afiliacion = Column(Boolean, server_default='false')
    creada = Column(DateTime, server_default=func.now())
    secciones = relationship(
        "Seccion",
        secondary="persona_seccion_link"
    )

    def __init__(self, *args, **kwargs):
        if kwargs.get("fecha_nacimiento") == "":
            kwargs["fecha_nacimiento"] = None
        super().__init__(*args, **kwargs)


class Seccion(IterableMixin, Base):
    __tablename__ = "secciones"

    id = Column(Integer, primary_key=True)
    nombre = Column(String(63), nullable=False)
    personas = relationship(
        "Persona",
        secondary="persona_seccion_link"
    )


class PersonaSeccionLink(Base):
    __tablename__ = "persona_seccion_link"
    persona_id = Column(Integer, ForeignKey("personas.id"), primary_key=True)
    seccion_id = Column(Integer, ForeignKey("secciones.id"), primary_key=True)
