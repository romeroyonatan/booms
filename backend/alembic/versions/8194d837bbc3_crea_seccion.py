"""Crea seccion

Revision ID: 8194d837bbc3
Revises: 1ee351049bc7
Create Date: 2018-07-11 16:20:26.532328

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8194d837bbc3'
down_revision = '1ee351049bc7'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'secciones',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('nombre', sa.String(63))
    )
    op.create_table(
        'persona_seccion_link',
        sa.Column('persona_id', sa.Integer, sa.ForeignKey("personas.id"),
                  primary_key=True),
        sa.Column('seccion_id', sa.Integer, sa.ForeignKey("secciones.id"),
                  primary_key=True)
    )


def downgrade():
    op.drop_table('seccion')
    op.drop_table('persona_seccion_link')
