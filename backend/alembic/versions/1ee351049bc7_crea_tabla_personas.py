"""Crea tabla personas

Revision ID: 1ee351049bc7
Revises:
Create Date: 2018-07-10 19:44:25.164610

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = '1ee351049bc7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'personas',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('nombre', sa.String, nullable=False),
        sa.Column('fecha_nacimiento', sa.Date),
        sa.Column('dni', sa.String(10)),
        sa.Column('email', sa.String(64)),
        sa.Column('telefono', sa.String(16)),
        sa.Column('celular', sa.String(16)),
        sa.Column('afiliacion', sa.Boolean, server_default='false'),
        sa.Column('creada', sa.DateTime, server_default=func.now()),
    )


def downgrade():
    op.drop_table("personas")
