import Vue from 'vue'
import Router from 'vue-router'
import PersonaList from '@/components/PersonaList'
import GrupoDetail from '@/components/GrupoDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PersonaList',
      component: PersonaList
    },
    {
      path: '/grupos/foo',
      name: 'GrupoDetail',
      component: GrupoDetail
    }
  ]
})
